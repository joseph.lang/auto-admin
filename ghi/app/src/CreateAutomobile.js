import React, { useEffect, useState } from "react";

const CreateAutomobile = () => {
  const [formData, setFormData] = useState({
    color: "",
    year: 0,
    vin: "",
    model_id: 0,
  });

  const [models, setModels] = useState([]);

  const getModels = async () => {
    const url = "http://localhost:8100/api/models/";

    const req = await fetch(url);

    if (req.ok) {
      const modelList = await req.json();
      setModels(modelList.models);
    }
  };

  useEffect(() => {
    getModels();
  }, []);

  const handleChange = (event) => {
    const name = event.target.name;
    const input = event.target.value;

    setFormData({
      ...formData,
      [name]: input,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8100/api/automobiles/";

    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-type": "application/json",
      },
    };

    const req = await fetch(url, fetchOptions);

    if (req.ok) {
      setFormData({
        color: "",
        year: 0,
        vin: "",
        model_id: 0,
      });
      alert("Automobile created successfully!");
    }
  };
  return (
    <>
      <form onSubmit={handleSubmit}>
        <h1>Create an Automobile</h1>
        <div className="mb-3">
          <label htmlFor="color" className="form-label">
            Color
          </label>
          <input
            type="text"
            className="form-control"
            aria-describedby="emailHelp"
            placeholder="Color"
            onChange={handleChange}
            value={formData.color}
            name="color"
          />
          <label htmlFor="year" className="form-label">
            Year
          </label>
          <input
            type="text"
            className="form-control"
            aria-describedby="emailHelp"
            placeholder="Year"
            onChange={handleChange}
            value={formData.year}
            name="year"
          />
          <label htmlFor="vin" className="form-label">
            Vin
          </label>
          <input
            type="text"
            className="form-control"
            aria-describedby="emailHelp"
            placeholder="Vin"
            onChange={handleChange}
            value={formData.vin}
            name="vin"
          />
          <label htmlFor="model_id" className="form-label">
            Choose a Model
          </label>
          <select
            className="form-select"
            name="model_id"
            value={formData.model_id}
            onChange={handleChange}
          >
            <option>Choose a Model</option>
            {models &&
              models.map((model) => (
                <option key={model.id} value={model.id}>
                  {model.name}
                </option>
              ))}
          </select>
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </>
  );
};

export default CreateAutomobile;
