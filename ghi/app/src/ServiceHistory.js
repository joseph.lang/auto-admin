import React, { useState, useEffect } from "react";

const ServiceHistory = () => {
  const [appointments, setAppointments] = useState([]);

  const [ourVins, setOurVins] = useState([]);

  const [searchValue, setSearchValue] = useState("");

  const updateSearch = (event) => {
    const value = event.target.value;
    setSearchValue(value);
  };

  const getFilteredAppointments = (searchValue, appointments) => {
    if (!appointments) {
      return appointments;
    } else {
      return appointments.filter((appointment) =>
        appointment.vin.includes(searchValue.toUpperCase())
      );
    }
  };

  const filteredAppointments = getFilteredAppointments(
    searchValue,
    appointments
  );

  const getVins = async () => {
    const url = "	http://localhost:8080/api/vins/";

    const req = await fetch(url);

    if (req.ok) {
      const vinsList = await req.json();
      setOurVins(vinsList.vins);
    }
  };

  useEffect(() => {
    getVins();
  }, []);

  const getAppointments = async () => {
    const url = "http://localhost:8080/api/appointments/";

    const req = await fetch(url);

    if (req.ok) {
      const appointmentsList = await req.json();

      setAppointments(appointmentsList.appointments);
    }
  };

  useEffect(() => {
    getAppointments();
  }, []);

  return (
    <>
      <h1>Service History</h1>
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="Search a vin"
          onChange={updateSearch}
        />
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.length > 0 ? (
            filteredAppointments.map((appointment) => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{ourVins.includes(appointment.vin) ? "yes" : "no"}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                <td> {new Date(appointment.date_time).toLocaleDateString()}</td>
                <td>
                  {appointment.technician.first_name}
                  {appointment.technician.last_name}
                </td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan={8}>
                <p>No results found</p>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </>
  );
};

export default ServiceHistory;
