import React, { useState, useEffect } from "react";

const CreateModel = () => {
  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: 0,
  });

  const [manufacturers, setManufacturers] = useState([]);

  const getManufacturers = async () => {
    const url = "http://localhost:8100/api/manufacturers/";

    const req = await fetch(url);

    if (req.ok) {
      const manufacturersList = await req.json();

      setManufacturers(manufacturersList.manufacturers);
    }
  };

  useEffect(() => {
    getManufacturers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8100/api/models/";

    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-type": "application/json",
      },
    };

    const req = await fetch(url, fetchOptions);

    if (req.ok) {
      alert("Model created successfully!");

      setFormData({
        name: "",
        picture_url: "",
        manufacturer_id: 0,
      });
    }
  };

  const handleChange = (event) => {
    const name = event.target.name;
    const input = event.target.value;

    setFormData({
      ...formData,
      [name]: input,
    });
  };
  return (
    <>
      <form onSubmit={handleSubmit}>
        <h1>Create a Model</h1>
        <div className="mb-3">
          <label htmlFor="picture_url" className="form-label">
            Model
          </label>
          <input
            type="text"
            className="form-control"
            aria-describedby="emailHelp"
            placeholder="Model Name"
            onChange={handleChange}
            value={formData.name}
            name="name"
          />
          <label htmlFor="picture_url" className="form-label">
            Picture Url
          </label>
          {formData.picture_url.length > 200 ? (
            <>
              <input
                type="text"
                className="form-control"
                placeholder="Picture URL"
                onChange={handleChange}
                value={formData.picture_url}
                name="picture_url"
              />
              <p className="text-danger">URL cannot exceed 200 characters</p>
            </>
          ) : (
            <input
              type="text"
              className="form-control"
              placeholder="Picture URL"
              onChange={handleChange}
              value={formData.picture_url}
              name="picture_url"
            />
          )}

          <label htmlFor="manufacturer_id" className="form-label">
            Choose a Manufacturer
          </label>
          <select
            className="form-select"
            name="manufacturer_id"
            value={formData.manufacturer_id}
            onChange={handleChange}
          >
            <option>Choose a Manufacturer</option>
            {manufacturers &&
              manufacturers.map((manufacturer) => (
                <option key={manufacturer.id} value={manufacturer.id}>
                  {manufacturer.name}
                </option>
              ))}
          </select>
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </>
  );
};

export default CreateModel;
