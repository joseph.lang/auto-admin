import React, { useEffect, useState } from "react";

const CreateServiceAppointment = () => {
  const [technicians, setTechnicians] = useState([]);
  const [formData, setFormData] = useState({
    date: "",
    time: "",
    reason: "",
    vin: "",
    customer: "",
    technician: "",
  });

  const getTechnicians = async () => {
    const url = "http://localhost:8080/api/technicians/";

    const req = await fetch(url);

    if (req.ok) {
      const listTechnicians = await req.json();
      setTechnicians(listTechnicians.technicians);
    }
  };

  useEffect(() => {
    getTechnicians();
  }, []);

  const handleChange = (event) => {
    const name = event.target.name;
    const input = event.target.value;

    setFormData({
      ...formData,
      [name]: input,
    });
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "	http://localhost:8080/api/appointments/";

    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-type": "application/json",
      },
    };
    const req = await fetch(url, fetchOptions);
    if (req.ok) {
      setFormData({
        date: "",
        time: "",
        reason: "",
        vin: "",
        customer: "",
        technician: "",
      });
      alert("Appointment created successfully!");
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form onSubmit={handleSubmit}>
            <h1>Create an Appointment</h1>
            <div className="mb-3">
              <label htmlFor="vin" className="form-label">
                Automobile VIN
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="VIN"
                onChange={handleChange}
                value={formData.vin}
                name="vin"
              />
              <label htmlFor="customer" className="form-label">
                Customer
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="Full Name"
                onChange={handleChange}
                value={formData.customer}
                name="customer"
              />
              <label htmlFor="date" className="form-label">
                Date
              </label>
              <input
                type="date"
                className="form-control"
                onChange={handleChange}
                value={formData.date}
                name="date"
              />
              <label htmlFor="time" className="form-label">
                Time
              </label>
              <input
                type="time"
                className="form-control"
                onChange={handleChange}
                value={formData.time}
                name="time"
              />
              <label htmlFor="technician" className="form-label">
                Technician
              </label>
              <select
                className="form-select"
                name="technician"
                onChange={handleChange}
                value={formData.technician}
              >
                <option>Choose a Technician</option>
                {technicians &&
                  technicians.map((technician) => (
                    <option
                      value={technician.employee_id}
                      key={technician.employee_id}
                    >
                      {technician.first_name} {technician.last_name}
                    </option>
                  ))}
              </select>
              <label htmlFor="reason" className="form-label">
                Reason
              </label>
              <input
                type="text"
                className="form-control"
                onChange={handleChange}
                value={formData.reason}
                name="reason"
              />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateServiceAppointment;
