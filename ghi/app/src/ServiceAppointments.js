import React, { useEffect, useState } from "react";

const ServiceAppointments = () => {
  const [appointments, setAppointments] = useState([]);

  const [ourVins, setOurVins] = useState([]);

  const getVins = async () => {
    const url = "	http://localhost:8080/api/vins/";

    const req = await fetch(url);

    if (req.ok) {
      const vinsList = await req.json();
      setOurVins(vinsList.vins);
    }
  };

  useEffect(() => {
    getVins();
  }, []);

  const getAppointments = async () => {
    const url = "http://localhost:8080/api/appointments/";

    const req = await fetch(url);

    if (req.ok) {
      const appointmentsList = await req.json();

      setAppointments(appointmentsList.appointments);
    }
  };

  useEffect(() => {
    getAppointments();
  }, []);

  const filteredAppointments = appointments.filter(
    (appointment) => appointment.status === "created"
  );

  const setFinished = async (id) => {
    const url = `http://localhost:8080/api/appointments/${id}/finish/`;
    const fetchOptions = {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
      },
    };
    const req = await fetch(url, fetchOptions);

    if (req.ok) {
      alert("Appointment successfully completed!");
      getAppointments();
    }
  };
  const setCancelled = async (id) => {
    const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
    const fetchOptions = {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
      },
    };
    const req = await fetch(url, fetchOptions);

    if (req.ok) {
      alert("Appointment successfully cancelled!");
      getAppointments();
    }
  };

  return (
    <>
      <h1>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments ? (
            filteredAppointments.map((appointment) => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{ourVins.includes(appointment.vin) ? "yes" : "no"}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                <td> {new Date(appointment.date_time).toLocaleDateString()}</td>
                <td>
                  {appointment.technician.first_name}{" "}
                  {appointment.technician.last_name}
                </td>
                <td>{appointment.reason}</td>
                <td>
                  <button
                    className="btn btn-success"
                    onClick={() => setFinished(appointment.id)}
                    style={{ marginRight: "0.5rem" }}
                  >
                    Finish
                  </button>
                  <button
                    className="btn btn-danger"
                    onClick={() => setCancelled(appointment.id)}
                  >
                    Cancel
                  </button>
                </td>
              </tr>
            ))
          ) : (
            <h3>There are not appointments at this time</h3>
          )}
        </tbody>
      </table>
    </>
  );
};

export default ServiceAppointments;
