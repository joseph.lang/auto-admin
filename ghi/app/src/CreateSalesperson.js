import { useState } from 'react';

export default function CreateSalesperson() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');


    const handleFirstNameChange = (e) => {
    const value = e.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (e) => {
        const value = e.target.value;
        setEmployeeId(value);
    }


    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId,
        };

        const salespersonUrl = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salespersonUrl, fetchConfig)
        if (response.ok) {
            alert("Salesperson created successfully!");
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        };
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Salesperson</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input
                            onChange={handleFirstNameChange}
                            value={firstName}
                            placeholder="First Name"
                            required type="text"
                            name="first_name"
                            id="first_anem"
                            className="form-control"/>
                            <label htmlFor="first_name">First Name</label>
                        </div>
                    <div className="form-floating mb-3">
                        <input
                            onChange={handleLastNameChange}
                            value={lastName}
                            placeholder="Last name"
                            required type="text"
                            name="last_name"
                            id="last_name"
                            className="form-control"/>
                            <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        {employeeId.length > 10 ?
                        <>
                         <input
                         onChange={handleEmployeeIdChange}
                         value={employeeId}
                         placeholder="Employee Id"
                         required type="text"
                         name="employee_id"
                         id="employee_id"
                         className="form-control"/>
                         <label htmlFor="employee_id">Employee Id</label>
                         <p className='text-danger'>Employee ID cannot exceed 10 characters</p>
                         </>
                          : (
                            <>
                             <input
                             onChange={handleEmployeeIdChange}
                             value={employeeId}
                             placeholder="Employee Id"
                             required type="text"
                             name="employee_id"
                             id="employee_id"
                             className="form-control"/>
                             <label htmlFor="employee_id">Employee Id</label>
                             </>
                        )}
                    </div>
                        <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
    );
};
