from django.db import models

# Create your models here.
class Technician (models.Model):
  first_name = models.CharField(max_length=200)
  last_name = models.CharField(max_length=200)
  employee_id = models.CharField(max_length=10, unique=True)

class AutomobileVO(models.Model):
  vin = models.CharField(max_length=17, unique=True)
  sold = models.BooleanField(default=False)

class Appointment(models.Model):
  date_time = models.DateTimeField()
  reason = models.CharField(max_length=500)
  status = models.CharField(max_length=200, default="created")
  vin = models.CharField(max_length=17, unique=True)
  customer = models.CharField(max_length=300)
  technician = models.ForeignKey(Technician, on_delete=models.CASCADE)
