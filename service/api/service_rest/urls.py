from django.urls import path
from .views import api_list_technicians, api_list_appointments, set_appointment_cancelled, set_appointment_finished, get_vins
urlpatterns = [
  path("technicians/", api_list_technicians, name="api_list_technicians"),
  path("appointments/", api_list_appointments, name="api_list_appointments"),
  path("appointments/<int:id>/cancel/", set_appointment_cancelled, name="cancel_appointment"),
  path("appointments/<int:id>/finish/", set_appointment_finished, name="finish_appointment"),
  path("vins/", get_vins, name="auto_vo_data")
]
